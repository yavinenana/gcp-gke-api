#!/bin/bash
# *** RUN LIKE TO SUDO ***
# Add the cloud SDK distribution URI as a package source
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
# apt-transport-https installed:
apt-get install apt-transport-https ca-certificates gnupg

# import google cloud public key
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

# update and install gcloud
apt-get update -y && apt-get install google-cloud-sdk
