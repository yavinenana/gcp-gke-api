# gcp-gke-api

* ![google cloud platform project - resources](img/hero-cloud-sdk.png?raw=true "GCP")

INSTALL 
# install docker without sudo
- ./install-docker.sh 
# Add the Cloud SDK distribution URI as a package source
# Import the Google Cloud Platform public key --- RUN SCRIPT like to sudo 
- sudo ./install-gcloud-sdk.sh 
# install gcloud kubectl    
- gcloud components install kubectl

1. First run create-projects-gcp.sh $email-account - to create PROJECT in GCP 
2. Secnd run create-service-accounts-gcp.sh $email-account - to create service account in GCP
3. Enable in "https://console.developers.google.com/project/$CODE-PROJECT/billing"  --- to enable billing
4. Third run push-dockerimage-gcr.sh $email-account - to push image docker to GOOGLE CLOUD REGISTRY



--------------------------------------------------------------------------------------------------------------------------------------
TASK 1:
 
Crear un script bash o makefile, que acepte parámetros (CREATE, DESTROY y OUTPUT) con los siguientes pasos:
Exportar las variables necesarias para crear recursos en GCP (utilizar las credenciales previamente descargadas).
Utilizar terraform o pulumi para crear un Cluster de Kubernetes de un solo nodo (GKE).
Instalar ingress controller en el Cluster de k8s.
Crear una imagen docker para desplegar una aplicación tipo RESTFUL API, basada en python que responda a siguientes dos recursos:
/greetings: message —> “Hello World from $HOSTNAME”.
/square: message —>  number: X, square: Y, donde Y es el cuadrado de X. Se espera un response con el cuadrado.
Subir la imagen el registry propio del proyecto gcp ej: gcr.io/$MYPROJECT/mypythonapp.
Desplegar la imagen con los objetos mínimos necesarios (no utilizar pods ni replicasets directamente).
El servicio debe poder ser consumido públicamente.

NOTA: variabilizar todos los campos que lo ameritan, por ejemplo el PROJECT, para que el script pueda ser ejecutado por otra persona con otra cuenta GCP.
---------------------------------------------------------------------------------------------------------------------------------------
TASK 2:
Crear un script bash o makefile, con los siguientes pasos:
Exportar las variables necesarias para crear recursos en GCP (utilizar las credenciales previamente descargadas).
Utilizar terraform o pulumi para crear un Cluster de Kubernetes de un solo nodo (GKE).
Crear una VM basada en Centos
Instalar Jenkins en la VM (Puede ser Instalado con Docker o como Servicio, pero es importante que la instalación se realice a través de un playbook de ansible)
Instalar plugins estándar de pipeline,
Crear un sharedlib que pueda compilar maven.
Crear un Job que haga uso del sharedlib para compilar exitosamente un proyecto java simple tipo “Hello World”
El repositorio para la aplicación de Java debe ser publico.
Nota: Todo debe realizarse de manera automática y que sea idempotente, para que cualquier persona pueda alcanzar el mismo resultado con sus propia cuentas, projects y credenciales. En el uso de ansible se penalizara para la evaluación la no utilización de los módulos especializados y por su sustitución de modelos genéricos como shell y command.
Cualquier consulta puedes llamarme o escribirme para solucionarlo 
