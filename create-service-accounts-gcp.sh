#!/bin/bash
# SERVICE ACCOUNT && give permissions with add-iam-policy-binding
# --- WARN --- REMEMBER THAT ... before you should enable billing in this new project
# Then enable work with kubernetes, so for it that you must ENABLE account billing - TO ALLOW PUSH IMAGE ----- 
# Enable in "https://console.developers.google.com/project/$CODE-PROJECT/settings"  --- to enable billing
# https://console.cloud.google.com/apis/api/containerregistry.googleapis.com/overview?project=$PROJECT

# $ ./create-projects-gcp.sh
source $PWD/values
echo $EMAIL_ACCOUNT
echo 'CREATING GCP PROJECT ...'
echo 'PROJECT:     '$PROJECT
echo 'PROJECT NAME:     '$PROJECTNAME
echo 'VERSION:        '$VERSION 
echo 'USER GCP: '$USER
echo 'DISPLAYUSER GCP: '$DISPLAY_USER
gcloud config set project $PROJECT

# Create Service Account
gcloud iam service-accounts create $USER \
    --display-name $DISPLAY_USER \
    --description "description test" \
    --project $PROJECT
# Create JSON KEY - of service account
gcloud iam service-accounts keys create $PWD/key-$PROJECT.json \
    --iam-account $USER@$PROJECT.iam.gserviceaccount.com
cat $PWD/key-$PROJECT.json

# give policy permissions to Service Accounts
gcloud projects add-iam-policy-binding $PROJECT \
        --member serviceAccount:$USER@$PROJECT.iam.gserviceaccount.com \
        --role roles/owner
# enable service modules of gcloud
gcloud services enable iam.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable containerregistry.googleapis.com
gcloud services enable container.googleapis.com

gcloud iam service-accounts list --project $PROJECT
gcloud auth activate-service-account --key-file=$PWD/key-$PROJECT.json 
#gcloud config set project $PROJECT

# Then enable work with kubernetes, so for it that you must ENABLE account billing - TO ALLOW PUSH IMAGE ----- 
# Enable in "https://console.developers.google.com/project/$CODE-PROJECT/settings"  --- to enable billing
# https://console.cloud.google.com/apis/api/containerregistry.googleapis.com/overview?project=$PROJECT
echo '----------------------------------finish-----------------------------------'
