#!/bin/bash
# create PROJECT
# ------------- FIRST LOGIN WITH :    $ gcloud init --console-only --account $EMAIL-ACCOUNT
# $ ./create-projects-gcp.sh $EMAIL-ACCOUNT
set -x
source $PWD/values
echo $EMAIL_ACCOUNT
echo 'CREATING GCP PROJECT ...'
echo 'PROJECT:     '$PROJECT
echo 'PROJECT NAME:     '$PROJECTNAME
echo 'VERSION:        '$VERSION 
echo 'USER GCP: '$USER
echo 'DISPLAYUSER GCP: '$DISPLAY_USER

gcloud init --console-only --account $EMAIL_ACCOUNT
gcloud projects create $PROJECT --name=$PROJECTNAME \
    --labels=type=tag1
gcloud projects describe $PROJECT
gcloud projects list
gcloud config set project $PROJECT
gcloud config set compute/zone us-west1
echo '.. so you must ENABLE account billing pls go to [and select LINK TO BILLING ACCOUNT]'
echo "https://console.developers.google.com/project/$PROJECT/billing"
echo '----------------------------------finish-----------------------------------'

#for project in  $(gcloud projects list --format="value(projectId)")
#do
#  echo "ProjectId:  $project"
#  for robot in $(gcloud iam service-accounts list --project $project --format="value(email)")
#   do
#     echo "    -> Robot $robot"
#     for key in $(gcloud iam service-accounts keys list --iam-account $robot --project $project --format="value(name.basename())")
#        do
#          echo "        $key"
#     done
#   done
#done

