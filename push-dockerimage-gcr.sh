#!/bin/bash
# PUSH TO GOOGLE CLOUD REGISTRY
source $PWD/values
echo $EMAIL_ACCOUNT
echo 'CREATING GCP PROJECT ...'
echo 'PROJECT:     '$PROJECT
echo 'PROJECT NAME:     '$PROJECTNAME
echo 'VERSION:        '$VERSION 
echo 'USER GCP: '$USER
echo 'DISPLAYUSER GCP: '$DISPLAY_USER

gcloud auth configure-docker
gcloud auth activate-service-account --key-file=$PWD/key-$PROJECT.json # here give permiss to push
gcloud config set project $PROJECT
docker build -t gcr.io/$PROJECT/$PROJECTNAME:v1 -f $PWD/docker-api-uflask/Dockerfile ./docker-api-uflask/
docker push gcr.io/$PROJECT/$PROJECTNAME:v1

echo '----------------------------------finish-----------------------------------'
