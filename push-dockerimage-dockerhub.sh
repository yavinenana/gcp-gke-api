#!/bin/bash
# PUSH TO DOCKERHUB
source $PWD/values.txt
echo 'BUILDING docker image ...'
echo 'IMAGE NAME:     '$IMAGENAME
echo 'VERSION:        '$VERSION 
echo 'USER DOCKERHUB: '$USER
docker build -t $IMAGENAME:$VERSION -f $PWD/docker-api-uflask/Dockerfile $PWD/docker-api-uflask
#docker build -t yavinenana/ --file Dockerfile .
docker tag $IMAGENAME:$VERSION $USER/$IMAGENAME:$VERSION
# if you've a commit to image , you should run
#docker commit -a "jordy peña - rest-python-flask <yavinenana@gmail.com>" -m "version 2.0" cont-python-flask yavinenana/rest-python-flask:1.0
#docker push yavinenana/rest-python-flask:1.0
docker push $USER/$IMAGENAME:$VERSION
echo '----------------------------------finish-----------------------------------'
