from flask import Flask
from flask import request
import operaciones as op
import socket

app = Flask(__name__)

app.config['DEBUG'] = True
app.config.update(
    TESTING=True,
    ENV=""
)
#def suma(a,b):
#  return a+b
#def resta(a,b):
#  return a-b
#
@app.route("/")
def hello():
    return "POJECT: gcp-gke-api - APIs: \n /api/v1/greeting/ - \n /api/v1/square/number/exponent"
    return " - /api/v1/suma/int1/int2"
    return " - /api/v1/resta/int1/int2"

@app.route("/api/v1/suma/<int:s1>/<int:s2>", methods=['GET','POST'])
def fun_suma(s1,s2):
    res = op.suma(s1,s2)	
    if request.method == "GET":
        content=request.get_json(silent=True)
#    return "!Hello my rest server - suma es: !\n" + str(res)
#    return "Hello World! %s" % (res)
#    return "Hello World!"
    return {"suma":res}

var_hostname=socket.gethostname()
@app.route("/api/v1/greeting/",methods=['GET','POST'])
def fun_greeting():
    res=op.message(var_hostname)
    return {"var hostname "+socket.gethostname():res}

@app.route("/api/v1/square/<int:number>/<int:exponent>", methods=['GET','POST'])
def fun_square(number,exponent):
    res=op.potency(number,exponent)
    if request.method == "GET":
        content=request.get_json(silent=True)
    return {"result of pontecy is: ":res}

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=5000)
